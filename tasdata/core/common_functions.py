#!/usr/bin/env python3
# coding=utf-8

'''
description:
常用方法模块
'''

import time
import datetime
import requests


def gettodaytime():
    '''
    获取当天0点时间'%Y-%m-%d %H:%M:%S'
    '''
    timetoday = time.strftime('%Y-%m-%d 00:00:00', time.localtime())
    return timetoday


def getyesterdaytime():
    '''
    获取昨日0点时间'%Y-%m-%d %H:%M:%S'
    '''
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    yesterdaytime = yesterday.strftime('%Y-%m-%d 00:00:00')
    return yesterdaytime


def gettimenow():
    '''
    获取当前时间'%Y-%m-%d %H:%M:%S'
    '''
    timenow = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return timenow


def getdaytime(day=0):
    '''
    获取历史时间'%Y-%m-%d %H:%M:%S'
    day=0代表当天，day=1代表昨日
    '''
    day = datetime.datetime.now() - datetime.timedelta(days=day)
    daytime = day.strftime('%Y-%m-%d 00:00:00')
    return daytime


def gethistime():
    '''
    获取一个很早的时间'%Y-%m-%d %H:%M:%S'
    '''
    histime = getdaytime(day=10000)
    return histime
