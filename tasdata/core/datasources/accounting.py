#!/usr/bin/env python3
# coding=utf-8

'''
description:
accounting数据库的数据统计
'''
# //eos若txid为空,才认为是未完成订单,如果调用回调成功txid有效了,即便状态为2,其值已可以被getbalance到,就会计算重复.
import pymysql
from tasdata.cores.common_data import envs
from tasdata.cores.common_functions import gettodaytime,gettimenow

class Accounting:
    '''
    accounting数据库的数据统计
    '''
    def __init__(self,*args):
        env = self.parsearguments(*args)
        self.hostname = envs[env]
    
    def getmysqlclient(self):
        '''
        获取连接172.31.47.23/trade_center
        '''
        db = pymysql.connect(
                host=self.hostname,
                port=3306,
                user='accounting',
                passwd='aad46b308c7e52f65dcc7ab2ffc24234',
                db='accounting',
                charset='utf8',
                cursorclass = pymysql.cursors.DictCursor)
        cursor = db.cursor()
        return cursor

    def parsearguments(self,*args):
        return 'test'
    
    def getexternalassets(self,cointype):
        '''
        获取外部仓数量
        如果没有配置的外部仓数据，则取10个仓的累和
        如果有配置过外部仓数据，则取json中的10个仓的累和
        select (json_extract(external,'$.ExtPositions[0]')+json_extract(external,'$.ExtPositions[1]')+json_extract(external,'$.ExtPositions[2]')+json_extract(external,'$.ExtPositions[3]')+json_extract(external,'$.ExtPositions[4]')+json_extract(external,'$.ExtPositions[5]')+json_extract(external,'$.ExtPositions[6]')+json_extract(external,'$.ExtPositions[7]')+json_extract(external,'$.ExtPositions[8]')+json_extract(external,'$.ExtPositions[9]')) as amount 
        from external_position 
        where coin_type='tcny';
    
        select (external_first+external_second+external_third+external_fourth+external_fifth+external_sixth+external_seventh+external_eighth+external_ninth+external_tenth) as 'amount' 
        from external_position 
        where coin_type='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select (external_first+external_second+external_third+external_fourth+external_fifth+external_sixth+external_seventh+external_eighth+external_ninth+external_tenth) as 'amount_default' 
        from external_position 
        where coin_type='{0}';
        '''.format(cointype)
        )
        amountdefault = cursor.fetchone()
        
        cursor.execute('''
        select (json_extract(external,'$.ExtPositions[0]')+json_extract(external,'$.ExtPositions[1]')+json_extract(external,'$.ExtPositions[2]')+json_extract(external,'$.ExtPositions[3]')+json_extract(external,'$.ExtPositions[4]')+json_extract(external,'$.ExtPositions[5]')+json_extract(external,'$.ExtPositions[6]')+json_extract(external,'$.ExtPositions[7]')+json_extract(external,'$.ExtPositions[8]')+json_extract(external,'$.ExtPositions[9]')) as amount_updated 
        from external_position 
        where coin_type='{0}';
        '''.format(cointype)
        )
        amountupdated = cursor.fetchone()
        if len(amountupdated) == 0:
            return float(amountdefault['amount_default']) if amountdefault['amount_default'] is not None else 0
        else:
            return float(amountupdated['amount_updated']) if amountupdated['amount_updated'] is not None else 0
        
