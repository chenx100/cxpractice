#!/usr/bin/env python3
# coding=utf-8

'''
description:

'''

from tasdata.cores.datasources.tradecenter import TradeCenter
from tasdata.cores.datasources.chain import Chain

class CrossData:
    '''
    跨库数据查询
    '''
    def getuserassets(self,cointype):
        '''
        获取散池资产
        '''
        tradecentersource = TradeCenter()
        useraddress = tradecentersource.getusercoinaddress(cointype)

        chainsource = Chain()
        userbalances = chainsource.getusercoinaddressbalances(cointype)

        userassets = 0
        for item in userbalances:
            for useraddr in useraddress:
                if item['address'] in useraddr['addr']:
                    userassets += item['balance']
        return userassets

    def getuserpoolassets(self,cointype):
        '''
        获取散池残留资产
        '''
        tradecentersource = TradeCenter()
        useraddress = tradecentersource.getusercoinaddress(cointype)

        chainsource = Chain()
        userbalances = chainsource.getusercoinaddressbalances(cointype)

        userassets = 0
        for item in userbalances:
            for useraddr in useraddress:
                if item['address'] in useraddr['addr']:
                    userassets += item['balance']
        return userassets