#!/usr/bin/env python3
# coding=utf-8

'''
description:
trade_center数据库的数据统计
'''
# //eos若txid为空,才认为是未完成订单,如果调用回调成功txid有效了,即便状态为2,其值已可以被getbalance到,就会计算重复.
import pymysql
from tasdata.cores.common_data import envs
from tasdata.cores.common_functions import gettodaytime,gettimenow

class TradeCenter:
    '''
    trade_center数据库的数据统计
    '''
    def __init__(self,*args):
        env = self.parsearguments(*args)
        self.hostname = envs[env]
    
    def getmysqlclient(self):
        '''
        获取连接172.31.47.23/trade_center
        '''
        db = pymysql.connect(
                host=self.hostname,
                port=3306,
                user='trade_center',
                passwd='aad46b308c7e52f65dcc7ab2ffc24234',
                db='trade_center',
                charset='utf8',
                cursorclass = pymysql.cursors.DictCursor)
        cursor = db.cursor()
        return cursor

    def parsearguments(self,*args):
        return 'test'
    
    def getdepositinprogress(self,cointype):
        '''
        获取充币在途数量
        1. 交易类型4（Order_DEPOSIT_COIN）和交易类型6（Order_CHAIN_IN）都算充币
        2. 订单状态2（Order_UNCOMPLETE）为未完成订单
        3. 分币种查询
        select sum(amount) as 'amount' 
        from trade_order 
        where trade_type in (4,6) 
        and status=2 
        and coin_type='tcny'
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount' 
        from trade_order 
        where trade_type in (4,6) 
        and status=2 
        and coin_type='{0}'
        and user_id<4284967295;
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0
    
    def getusercoinaddress(self,cointype):
        '''
        获取币种所有地址
        未统计子钱包
        select distinct(addr) 
        from coin_address 
        where coin_type='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select user_id,addr 
        from coin_address 
        where coin_address.coin_type='{0}' 
        and user_id<4284967295;
        '''.format(cointype)
        )
        userandaddrs = cursor.fetchall()
        return userandaddrs

    def getcoldpoolassets(self,cointype):
        '''
        获取币种冷池资产
        1. 订单行为是入库（entering）
        2. 订单状态是完成(0)
        3. 订单币种是查询币种
        select sum(amount) as amount 
        from internal_order 
        where coin_type='tcny' 
        and action='entering' 
        and status=0;
        '''
        cursor = self.getmysqlclient()

        cursor.execute('''
        select sum(amount) as amount 
        from internal_order 
        where coin_type='{0}' 
        and action='entering' 
        and status=0
        and user_id<4284967295;        
        '''.format(cointype))
        coldpoolasset = cursor.fetchone()
        return float(coldpoolasset['amount']) if coldpoolasset['amount'] is not None else 0

    def getbuycoinamount(self,cointype,fromtime,totime):
        '''
        获取买币数量和买币金额
        包含买币（Order_BUY，订单类型0）和补发币（Order_REISSUE，订单类型15）
        订单状态为成功
        select sum(amount) as 'amount',sum(cash) as 'cash'
        from trade_order 
        where trade_type in (0,15) 
        and status=0 
        and coin_type='tcny'
        and finish_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount',sum(cash) as 'cash'
        from trade_order 
        where trade_type in (0,15) 
        and status=0 
        and finish_time>'{0}' and finish_time<'{1}' 
        and coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0,float(amount['cash']) if amount['cash'] is not None else 0

    def getsellcoinamount(self,cointype,fromtime,totime):
        '''
        获取卖币数量和卖币金额
        包含卖币（Order_SELL，订单类型1）
        订单状态为成功
        select sum(amount) as 'amount',sum(cash) as 'cash'
        from trade_order 
        where trade_type=1
        and status=0 
        and coin_type='tcny'
        and finish_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount',sum(cash) as 'cash'  
        from trade_order 
        where trade_type=1
        and status=0 
        and finish_time>'{0}' and finish_time<'{1}' 
        and coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0,float(amount['cash']) if amount['cash'] is not None else 0

    def getcommissionamount(self,cointype,fromtime,totime):
        '''
        获取买币/卖币/补发币的佣金
        包含买币（Order_BUY，订单类型0）和补发币（Order_REISSUE，订单类型15）和卖币（Order_SELL，订单类型1）
        订单状态为成功
        select sum(tip) as 'amount'
        from trade_order 
        where trade_type in (0,1,15) 
        and status=0 
        and coin_type='tcny'
        and finish_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(tip) as 'amount' 
        from trade_order 
        where trade_type in (0,1,15) 
        and status=0 
        and finish_time>'{0}' and finish_time<'{1}' 
        and coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getdepositcoinamount(self,cointype,fromtime,totime):
        '''
        获取充币数量
        包含充币（Order_DEPOSIT_COIN，订单类型4）和链上转入（Order_CHAIN_IN，订单类型6）
        select sum(amount) as 'amount'
        from trade_order 
        where trade_type in (4,6)
        and status=0 
        and coin_type='tcny'
        and create_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount'
        from trade_order 
        where trade_type in (4,6)
        and status=0 
        and create_time>'{0}' and create_time<'{1}' 
        and coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getwithdrawamount(self,cointype,fromtime,totime):
        '''
        获取提币数量
        包含提币（Order_WITHDRAW_COIN，订单类型5）
        交易状态为成功（Order_SUCC，订单状态0）
        交易状态为未完成，但tx_id已生成（Order_UNCOMPLETE，订单状态2，tx_id不为空）
        select sum(amount) as 'amount',sum(cash) as 'cash'
        from trade_order 
        where trade_type=5
        and (status=0 or (status=2 and tx_id is not null))
        and coin_type='tcny'
        and create_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount'
        from trade_order 
        where trade_type=5
        and (status=0 or (status=2 and tx_id is not null)) 
        and create_time>'{0}' and create_time<'{1}' 
        and coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getwithdrawrealfee(self,cointype,fromtime,totime):
        '''
        获取提币矿工费
        包含提币（Order_WITHDRAW_COIN，订单类型5）
        交易状态为成功（Order_SUCC，订单状态0）
        交易状态为未完成，但tx_id已生成（Order_UNCOMPLETE，订单状态2，tx_id不为空），且确认次数大于等于1次（confirm>=1）
        usdt提币消耗btc
        tcny提币消耗eth
        select sum(real_fee) as 'amount'
        from trade_order 
        where trade_type=5
        and (status=0 or (status=2 and tx_id is not null and confirm>=1))
        and coin_type='tcny'
        and create_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()

        if cointype.lower() == 'btc':
            cointype = '''
            'btc','usdt'
            '''
        elif cointype.lower() == 'eth':
            cointype = '''
            'eth','tcny'
            '''
        elif cointype.lower() in ['eos','neo','ont','tcny','usdt']:
            cointype = '''
            'invalidcoin'
            '''
        else:
            cointype = '''
            '{0}'
            '''.format(cointype)

        cursor.execute('''
        select sum(real_fee) as 'amount'
        from trade_order 
        where trade_type=5
        and (status=0 or (status=2 and tx_id is not null and confirm>=1)) 
        and create_time>'{0}' and create_time<'{1}' 
        and coin_type in ({2})
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getflashexchangeinamount(self,cointype,fromtime,totime):
        '''
        获取闪兑转入数量
        交易类型为闪兑（9）
        交易状态为成功（0）
        币种为选择币种
        交易完成时间在选择区间
        select sum(amount) as 'in'
        from trade_order 
        where trade_type=9
        and status=0 
        and coin_type='tcny'
        and finish_time between (date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'in'
        from trade_order 
        where trade_type=9
        and status=0
        and finish_time>'{0}' and finish_time<'{1}' 
        and coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['in']) if amount['in'] is not None else 0

    def getflashexchangeoutamount(self,cointype,fromtime,totime):
        '''
        获取闪兑转出数量
        交易类型为闪兑（9）
        交易状态为成功（0）
        币种为选择币种
        交易完成时间在选择区间
        select sum(base_amount) as 'out' 
        from trade_order 
        where trade_type=9
        and status=0 
        and base_coin_type='tcny'
        and finish_time between (date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(base_amount) as 'out' 
        from trade_order 
        where trade_type=9
        and status=0
        and finish_time>'{0}' and finish_time<'{1}' 
        and base_coin_type='{2}'
        and user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['out']) if amount['out'] is not None else 0

    def getflashexchangefee(self,cointype,fromtime,totime):
        '''
        获取闪兑服务费
        包含闪兑交易（Order_FLASH_EXCHANGE，订单类型9）
        交易状态为成功（Order_SUCC，订单状态0）
        select sum(trade_order.amount/(1-json_extract(flash_price.raw_json,'$.weight'))*json_extract(flash_price.raw_json,'$.weight')) as 'amount'
        from trade_order left join flash_price.id on trade_order.price_id=flash_price.id
        where trade_order.trade_type=9
        and trade_order.status=0
        and trade_order.coin_type='tcny'
        and trade_order.finish_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(trade_order.amount/(1-json_extract(flash_price.raw_json,'$.weight'))*json_extract(flash_price.raw_json,'$.weight')) as 'amount'
        from trade_order left join flash_price on trade_order.price_id=flash_price.id
        where trade_order.trade_type=9
        and trade_order.status=0
        and trade_order.finish_time>'{0}' and trade_order.finish_time<'{1}' 
        and trade_order.coin_type='{2}'
        and trade_order.user_id<4284967295;
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getuseramount(self,cointype):
        '''
        获取用户币种数量
        币种为选择币种
        select sum(tcny_amount) as 'amount' 
        from account 
        where coin_type='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum({0}_amount) as 'amount' 
        from account 
        where user_id<4284967295;
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getuserhbbamount(self,cointype):
        '''
        获取用户活币宝币种数量
        币种为选择币种
        select sum(amount) as 'amount' 
        from hbb_account 
        where coin_type='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount' 
        from hbb_account 
        where coin_type='{0}'
        and user_id<4284967295;
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getuserloanamount(self,cointype):
        '''
        获取用户质押宝借入币币种数量
        币种为选择币种
        select sum(original_loan_amount) as 'amount' 
        from zyb_order 
        where status=0
        loan_coin='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(original_loan_amount) as 'amount' 
        from zyb_order 
        where status=0
        and loan_coin='{0}'
        and user_id<4284967295;
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getplatformmortgageamount(self,cointype):
        '''
        获取用户质押宝抵押币币种数量
        币种为选择币种
        select sum(current_mortgage_amount) as 'amount' 
        from zyb_order 
        where status=0
        and mortgage_coin='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(current_mortgage_amount) as 'amount' 
        from zyb_order 
        where status=0
        and mortgage_coin='{0}'
        and user_id<4284967295;
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getuserinterestamount(self,cointype):
        '''
        获取用户已完成质押宝订单币种息费数量
        币种为选择币种
        select sum(mortgage_loan_total_interest) as 'amount' 
        from zyb_order 
        where status!=0
        loan_coin='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(mortgage_loan_total_interest) as 'amount' 
        from zyb_order 
        where status!=0
        and loan_coin='{0}'
        and user_id<4284967295;
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0        

    def getenteringinprogress(self,cointype):
        '''
        获取平台入库在途订单币种数量
        币种为选择币种
        select sum(amount) as 'amount' 
        from internal_order 
        where status=2
        and action='entering'
        and finish_time>'{0}' and finish_time<'{1}' 
        coin_type='tcny';
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount' 
        from internal_order 
        where status=2
        and coin_type='{0}';
        '''.format(cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0        

    def getassemblingamount(self,cointype,fromtime,totime):
        '''
        获取归集币种数量
        包含归集（订单action “assembling”）
        交易状态为成功（Order_SUCC，订单状态0）
        select sum(amount) as 'amount'
        from internal_order 
        where action='assembling' 
        and status=0
        and coin_type='tcny'
        and create_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount'
        from internal_order 
        where action='assembling' 
        and status=0
        and create_time>'{0}' and create_time<'{1}' 
        and coin_type='{2}';
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getassemblingcostamount(self,cointype,fromtime,totime):
        '''
        获取归集币种损耗数量
        包含归集（订单action “assembling”）
        交易状态为成功（Order_SUCC，订单状态0）
        select sum(fee) as 'amount'
        from internal_order 
        where action='assembling' 
        and status=0
        and coin_type='tcny'
        and create_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(fee) as 'amount'
        from internal_order 
        where action='assembling' 
        and status=0
        and create_time>'{0}' and create_time<'{1}' 
        and coin_type='{2}';
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getcoverpoolamount(self,cointype,fromtime,totime):
        '''
        获取补仓币种数量
        包含归集（订单action “coverhootpool”）
        交易状态为成功（Order_SUCC，订单状态0）
        select sum(amount) as 'amount'
        from internal_order 
        where action='coverhootpool' 
        and status=0
        and coin_type='tcny'
        and create_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(amount) as 'amount'
        from internal_order 
        where action='coverhootpool' 
        and status=0
        and create_time>'{0}' and create_time<'{1}' 
        and coin_type='{2}';
        '''.format(fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return float(amount['amount']) if amount['amount'] is not None else 0

    def getordercount(self,cointype,ordertype,fromtime,totime):
        '''
        获取订单数量
        select count(*) as 'amount'
        from trade_order 
        where trade_type=0 
        and coin_type='tcny'
        and finish_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(*)  as 'amount'
        from trade_order 
        where trade_type={0} 
        and finish_time>'{1}' and finish_time<'{2}' 
        and coin_type='{3}'
        and user_id<4284967295;
        '''.format(ordertype,fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return int(amount['amount']) if amount['amount'] is not None else 0

    def getpaysuccesscount(self,cointype,ordertype,fromtime,totime):
        '''
        获取支付成功订单数量
        PaymentChannelType_VIP             PaymentChannelType = 6
        Order_SUCC                       Order_OrderStatus = 0
        select count(*) as 'amount'
        from trade_order inner join cash_order on trade_order.order_id=cash_order.order_id
        where trade_order.trade_type=0 
        and trade_order.coin_type='tcny'
        and ((trade_order.payment_channel_type=6 and trade_order.status=0) or cash_order.status='pingbackOK')
        and cash_order.create_at between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(*)  as 'amount'
        from trade_order  inner join cash_order on trade_order.order_id=cash_order.order_id
        where trade_order.trade_type={0} 
        and cash_order.create_at>'{1}' and cash_order.create_at<'{2}' 
        and trade_order.coin_type='{3}'
        and ((trade_order.payment_channel_type=6 and trade_order.status=0) or cash_order.status='pingbackOK')
        and trade_order.user_id<4284967295;
        '''.format(ordertype,fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return int(amount['amount']) if amount['amount'] is not None else 0

    def getordersuccesscount(self,cointype,ordertype,fromtime,totime):
        '''
        获取成功订单数量
        Order_SUCC                       Order_OrderStatus = 0
        select count(*) as 'amount'
        from trade_order
        where trade_type=0 
        and trade_order.coin_type='tcny'
        and status=0
        and trade_order.finish_time between(date_add(curdate(),interval 0 hour),now());
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(*)  as 'amount'
        from trade_order
        where trade_type={0} 
        and finish_time>'{1}' and finish_time<'{2}' 
        and trade_order.coin_type='{3}'
        and status=0
        and trade_order.user_id<4284967295;
        '''.format(ordertype,fromtime,totime,cointype)
        )
        amount = cursor.fetchone()
        return int(amount['amount']) if amount['amount'] is not None else 0
    