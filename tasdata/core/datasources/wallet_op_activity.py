#!/usr/bin/env python3
# coding=utf-8

'''
description:
wallet_op_activity数据库的数据统计
'''
import pymysql
from tasdata.cores.common_data import envs
from tasdata.cores.common_functions import gettodaytime,gettimenow

class BoOrder:
    '''
    wallet_op_activity 数据库的数据统计
    '''
    def __init__(self,*args):
        env = self.parsearguments(*args)
        self.hostname = envs[env]
    
    def getmysqlclient(self):
        '''
        获取连接172.31.47.23/wallet_op_activity
        '''
        db = pymysql.connect(
                host=self.hostname,
                port=3306,
                user='wallet_op_activity',
                passwd='N1qUcd!16mWDzsLd',
                db='wallet_op_activity',
                charset='utf8',
                cursorclass = pymysql.cursors.DictCursor)
        cursor = db.cursor()
        return cursor

    def parsearguments(self,*args):
        return 'test'
    
    def getstakeuserexpandordercountexp(self,fromtime,totime):
        '''
        获取体验区下注人数和体验区下注次数
        select count(distinct(user_id)) as usercount,count(order_id) as 'ordercount'
        from bo_exp_order 
        where create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now()
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(distinct(user_id)) as usercount,count(order_id) as 'ordercount'
        from bo_exp_order 
        where create_at>'{0}' 
        and create_at<='{1}' 
        and user_id<4284967295;
        '''.format(fromtime,totime)
        )
        amount = cursor.fetchone()
        return int(amount['usercount']) if amount['usercount'] is not None else 0,int(amount['ordercount']) if amount['ordercount'] is not None else 0

    def getstakeuserandordercount(self,fromtime,totime):
        '''
        获取下注人数和下注次数
        select count(distinct(user_id)) as usercount,count(order_id) as 'ordercount'
        from bo_order 
        where create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now()
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(distinct(user_id)) as usercount,count(order_id) as 'ordercount'
        from bo_order 
        where create_at>'{0}' 
        and create_at<='{1}' 
        and user_id<4284967295;
        '''.format(fromtime,totime)
        )
        amount = cursor.fetchone()
        return int(amount['usercount']) if amount['usercount'] is not None else 0,int(amount['ordercount']) if amount['ordercount'] is not None else 0

    def getuserloseordercount(self,fromtime,totime):
        '''
        获取用户输的次数
        select count(order_id) as 'ordercount'
        from bo_order 
        where stake_result=2 
        and create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now();
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(order_id) as 'ordercount'
        from bo_order 
        where stake_result=2 
        and create_at>'{0}' 
        and create_at<='{1}' 
        and user_id<4284967295;
        '''.format(fromtime,totime)
        )
        amount = cursor.fetchone()
        return float(amount['ordercount']) if amount['ordercount'] is not None else 0

    def getstakeamountandplatformprofit(self,fromtime,totime):
        '''
        获取下注总额/平台盈利
        select sum(stake_count*flash_price) as 'stakeamount',sum(gross_profit*flash_price)*-1 as profitamount
        from bo_order 
        where create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now();
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select sum(stake_count*flash_price) as 'stakeamount',sum(gross_profit*flash_price)*-1 as profitamount
        from bo_order 
        where create_at>'{0}' 
        and create_at<='{1}' 
        and user_id<4284967295;
        '''.format(fromtime,totime)
        )
        amount = cursor.fetchone()
        return float(amount['stakeamount']) if amount['stakeamount'] is not None else 0,float(amount['profitamount']) if amount['profitamount'] is not None else 0

    def stakeupordowncount(self,upOrDown,fromtime,totime):
        '''
        获取用户猜涨（0）/猜跌的次数（1）
        select count(order_id) as count 
        from bo_order 
        where stake_type=0
        and create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now();
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(order_id) as count 
        from bo_order 
        where stake_type={0} 
        and create_at>'{1}' 
        and create_at<='{2}' 
        and user_id<4284967295;
        '''.format(upOrDown,fromtime,totime)
        )
        amount = cursor.fetchone()
        return int(amount['count']) if amount['count'] is not None else 0

    def stakelosecount(self,upOrDown,fromtime,totime):
        '''
        获取用户猜涨（0）/猜跌的次数（1）输（2）的次数
        select count(order_id) as count 
        from bo_order 
        where stake_type=0
        and stake_result=2
        and create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now();
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select count(order_id) as count 
        from bo_order 
        where stake_type={0} 
        and stake_result=2 
        and create_at>'{1}' 
        and create_at<='{2}' 
        and user_id<4284967295;
        '''.format(upOrDown,fromtime,totime)
        )
        amount = cursor.fetchone()
        return int(amount['count']) if amount['count'] is not None else 0

    def getreturnrates(self,fromtime,totime):
        '''
        获取最高/最低/平均返奖率
        select max(settlement_return),min(settlement_return),avg(settlement_return)
        from bo_order 
        where create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now();
        '''
        cursor = self.getmysqlclient()
        cursor.execute('''
        select max(settlement_return) as 'max',min(settlement_return) as 'min',avg(settlement_return) as 'avg'
        from bo_order 
        where create_at>'{0}' 
        and create_at<='{1}' 
        and user_id<4284967295;
        '''.format(fromtime,totime)
        )
        amount = cursor.fetchone()
        return float(amount['max']) if amount['max'] is not None else 0.0,float(amount['min']) if amount['min'] is not None else 0.0,float(amount['avg']) if amount['avg'] is not None else 0.0

    def getuserdimensiondata(self,fromtime,totime):
        '''
        获取用户维度数据
        select user_id,count(order_id) as 'ordercount'
        from bo_order 
        where create_at>date_add(curdate(),INTERVAL 0 hour) 
        and create_at<=now()
        '''
        
        pass

if __name__ == "__main__":
    pass