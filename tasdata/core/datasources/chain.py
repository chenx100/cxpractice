#!/usr/bin/env python3
# coding=utf-8

'''
description:
chain数据库的数据统计
'''
import pymysql
from tasdata.cores.common_data import chainenvs as envs
from tasdata.cores.common_functions import gettodaytime,gettimenow

class Chain:
    '''
    chain数据库的数据统计
    '''
    def __init__(self,*args):
        env = self.parsearguments(*args)
        self.hostname = envs[env]
    
    def getmysqlclient(self):
        '''
        获取连接172.31.47.23/chain
        '''
        db = pymysql.connect(
                host=self.hostname,
                port=3306,
                user='chain',
                passwd='x8G7Jdky8yo#aXC',
                db='chain',
                charset='utf8',
                cursorclass = pymysql.cursors.DictCursor)
        cursor = db.cursor()
        return cursor

    def parsearguments(self,*args):
        return 'test'
    
    def getusercoinaddressbalances(self,cointype):
        '''
        获取币种地址的资产
        select address,balance from chain_balance where coin_type='tcny' group by coin_type,address;
        '''
        cursor = self.getmysqlclient()

        cursor.execute(
           '''
            select address,balance from chain_balance where coin_type='{0}' group by coin_type,address;
           '''.format(cointype)
        )
        coinaddressbalances = cursor.fetchall()
        return coinaddressbalances
