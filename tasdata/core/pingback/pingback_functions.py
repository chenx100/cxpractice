#!/usr/bin/env python3
# coding=utf-8
import os
import sys

from tasdata.cores.pingback.mongo_query import PingbackMongoQuery
from tasdata.cores.common_data import countlykeys

def showpingbackdata(events,*argv):
    '''
    解析命令行参数
    '''
    pingbackquery = PingbackMongoQuery()
    fromtime,totime,_ = pingbackquery.parsearguments(*argv)
    
    for event in events:
        eventcount,eventusers = pingbackquery.queryeventandusercount(event,fromtime,totime)
        print(countlykeys[event]+'次数：'+str(eventcount),countlykeys[event]+'用户数：'+str(eventusers))