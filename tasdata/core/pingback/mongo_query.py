#!/usr/bin/env python3
# coding=utf-8
import pymongo
import time
import datetime
from tasdata.cores.common_data import envs

class PingbackMongoQuery:
    '''
    mongodb查询pingback数据工具类
    '''
    def __init__(self, *args):
        _,_,env = self.parsearguments(*args)
        self.hostname = envs[env]

    def getmongoclient(self): 
        '''
        获取连接172.31.47.23/pingback/logs
        '''  
        moclient = pymongo.MongoClient(
            host=self.hostname,
            port=37017,
            username='pingback',
            password='47c9b85507c22a4350154db8fc83d85a',
            authSource='pingback',
            authMechanism='SCRAM-SHA-1'
            )

        modb = moclient['pingback']
        mocol = modb['logs']
        return mocol
    
    def parsearguments(self,*args):
        '''
        解析命令行参数
        '''
        fromtime = int(time.mktime(time.strptime(time.strftime('%Y-%m-%d 00:00:00', time.localtime()),'%Y-%m-%d %H:%M:%S'))*1000)
        totime = int(time.time()*1000)
        env = 'test'
        return fromtime,totime,env

    def queryeventcount(self, eventkey, fromtime, totime):
        '''
        查询pingback的事件数量
        '''
        mocol = self.getmongoclient()
        # TODO:数据量小的时候先这样写，数据量超过 16 M，再考虑聚合
        # count = len(mocol.distinct("events.timestamp",{"events.key":{"$regex":"^{0}".format(eventkey)},"timestamp":{"$gt":fromtime,"$lte":totime}}))
        prefix, suffix = self.parseeventname(eventkey)
        logs = mocol.find({"events.key":{"$regex":"^{0}".format(prefix)},"user_id":{"$ne":""},"timestamp":{"$gt":fromtime,"$lte":totime}})

        count = 0
        for log in logs:
            for event in log['events']:
                if suffix == '':    
                    if event['key'].startswith(prefix):
                        count+=event['count']
                else:
                    if event['key'].startswith(prefix) and event['key'].endswith(suffix):
                        count+=event['count']                    

        return count

    def queryusercount(self, eventkey, fromtime, totime):
        '''
        查询pingback的用户数量
        '''
        mocol = self.getmongoclient()
        # 数据量小的时候先这样写，数据量超过 16 M，再考虑聚合
        prefix, suffix = self.parseeventname(eventkey)
        logs = mocol.find({"events.key":{"$regex":"^{0}".format(prefix)},"user_id":{"$ne":""},"timestamp":{"$gt":fromtime,"$lte":totime}})

        users = []
        for log in logs:
            for event in log['events']:
                if suffix == '':    
                    if event['key'].startswith(prefix):
                        if log['user_id'] not in users:
                            users.append(log['user_id'])
                else:
                    if event['key'].startswith(prefix) and event['key'].endswith(suffix):
                        if log['user_id'] not in users:
                            users.append(log['user_id'])                        
                 
        # count = len(mocol.distinct("user_id",{"events.key":{"$regex":"^{0}".format(eventkey)},"user_id":{"$ne":""},"timestamp":{"$gt":fromtime,"$lte":totime}}))
        return len(users)

    def queryeventandusercount(self, eventkey, fromtime, totime):
        '''
        查询pingback的事件数量和用户数量
        '''
        eventcount = self.queryeventcount(eventkey, fromtime, totime)
        usercount = self.queryusercount(eventkey, fromtime, totime)
        return eventcount, usercount

    def parseeventname(self,eventkey):
        '''
        处理特殊事件名称，如'mining_homepage_$id_click'/'wallet_HomeBanner_Show_ID'
        '''
        prefix = eventkey
        suffix = ''
        if eventkey.endswith('_ID'):
            prefix = eventkey[:-3]

        if '$id' in eventkey:
            eventparts = eventkey.split('$id')
            prefix, suffix = eventparts[0], eventparts[1]

        return prefix,suffix

if __name__ == '__main__':
    print("imported pingbackmongoquery class")